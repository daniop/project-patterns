from abc import ABC, abstractmethod
from keyboard import on_press
from time import sleep
from typing import List


class KeyInterface(ABC):

    @abstractmethod
    def get_key(self):
        pass

    @abstractmethod
    def rise_message(self):
        pass


class KeyboardInterface(ABC):

    @abstractmethod
    def add(self, key: KeyInterface) -> None:
        pass

    @abstractmethod
    def remove(self, key: KeyInterface) -> None:
        pass

    @abstractmethod
    def check_key(self, event) -> None:
        pass


class Keyboard(KeyboardInterface):

    def __new__(cls):
        if not hasattr(cls, 'instance'):
            print('Creating new instance...')
            cls.instance = super(Keyboard, cls).__new__(cls)
        return cls.instance

    def __init__(self) -> None:
        self.run = True
        self.list_of_key: List[KeyInterface] = []

    def add(self, key: KeyInterface) -> None:
        if key not in self.list_of_key:
            self.list_of_key.append(key)

    def remove(self, key: KeyInterface) -> None:
        try:
            self.list_of_key.remove(key)
        except ValueError as err:
            print(f'Can\'t remove {key}')
            print(err)

    def check_key(self, event) -> None:
        if event.name == 'esc':
            print("stopping keyboard")
            self.run = False

        for x in self.list_of_key:
            if event.name == x.get_key():
                self.remove(x)
                x.rise_message()
                print(f'Remove {x}')

    def start_keyboard_check(self) -> None:
        print("Starting keyboard, press ESC to stop program")
        self.run = True
        on_press(self.check_key)
        while self.run:
            sleep(1)


class Key(KeyInterface):

    def __init__(self, key: str) -> None:
        self.key: str = key

    def get_key(self) -> str:
        return self.key

    def rise_message(self):
        print(f"Wciśnięto klawisz {self.key}")


class KeyExclamation(KeyInterface):

    def __init__(self, key: Key) -> None:
        self.key_object: Key = key

    def get_key(self) -> str:
        return self.key_object.get_key()

    def rise_message(self):
        print(f"Wciśnięto klawisz {self.key_object.get_key()}!")


class KeyQuestionMark(KeyInterface):

    def __init__(self, key: Key) -> None:
        self.key_object: Key = key

    def get_key(self) -> str:
        return self.key_object.get_key()

    def rise_message(self):
        print(f"Wciśnięto klawisz {self.key_object.get_key()}?")


def main() -> None:
    keyboard = Keyboard()
    keyboard1 = Keyboard()

    print(keyboard1 == keyboard)

    key1 = KeyExclamation(Key("a"))
    key2 = KeyQuestionMark(Key("b"))
    key3 = Key("c")
    key4 = Key("d")
    key5 = Key("e")

    keyboard.add(key1)
    keyboard.add(key2)
    keyboard.add(key3)
    keyboard.add(key4)
    keyboard.add(key5)

    print("-------------")

    keyboard.start_keyboard_check()


if __name__ == "__main__":
    main()
