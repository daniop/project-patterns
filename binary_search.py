from math import floor


def main() -> None:
    rand_list: list = [x for x in range(20, 2000, 2)]

    res = bin_search(rand_list, len(rand_list), 444)

    if res > 0:
        print(f"Wynik = {res} ----> {rand_list[res]}")

    if res == -1:
        print(f"Brak szukanej w liście ----> {res}")


def bin_search(search_list: list, n: int, p: int) -> int:
    left_border: int = 0
    right_border: int = n - 1

    if p < search_list[left_border] or p > search_list[right_border]:
        return -1

    while left_border <= right_border:

        res = floor((left_border + right_border) / 2)

        if p < search_list[res]:
            right_border = res - 1

        if p > search_list[res]:
            left_border = res + 1

        if p == search_list[res]:
            return res

    return -1


if __name__ == "__main__":
    main()
